/*
  type param expects 'data', 'message'
*/

module.exports = {
  unwrapData: function (json) {
    if (json) {
      var data = json.data.response.data
      return data
    } else {
      console.warn('jsonResponse.unwrapData() failed to complete')
    }
  },
  unwrapMessage: function (json) {
    if (json) {
      var message = json.data.response.message
      return message
    } else {
      console.warn('jsonResponse.unwrapMessage() failed to complete')
    }
  }
}
