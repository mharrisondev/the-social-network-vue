module.exports = {
  request: function (req, token) {
    this.options.http._setHeaders.call(this, req, {Authorization: 'Bearer ' + token})
  },
  response: function (res) {
    // console.log('res', res)
    if (res.data.response && res.data.response.data) {
      var token = res.data.response.data.token
      if (token) {
        return token
      }
    }
  }
}
