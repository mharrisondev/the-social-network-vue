// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import 'bulma/css/bulma.css'
import axios from 'axios'
import VueAxios from 'vue-axios'

// setup vue router for vue-auth
Vue.router = router

// setup axios ajax
Vue.use(VueAxios, axios)
Vue.axios.defaults.baseURL = process.env.API_URL

// setup vue-auth
Vue.use(require('@websanova/vue-auth'), {
  auth: require('./auth.js'), // custom 'driver'
  http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
  router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
  parseUserData: function (data) {
    if (data && data.response) {
      return data.response.data
    } else {
      return {}
    }
  }
})

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
