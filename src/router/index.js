import Vue from 'vue'
import Router from 'vue-router'
import pageHome from '@/components/pageHome'
import pageProfile from '@/components/pageProfile'
import pageTimeline from '@/components/pageTimeline'
import pageLogin from '@/components/pageLogin'
import pageRecoverPassword from '@/components/pageRecoverPassword'
import pageNewPassword from '@/components/pageNewPassword'
import pageRegister from '@/components/pageRegister'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: pageHome
    },
    {
      path: '/profile',
      name: 'Profile',
      component: pageProfile
    },
    {
      path: '/login',
      name: 'Login',
      component: pageLogin
    },
    {
      path: '/recover-password',
      name: 'Recover Password',
      component: pageRecoverPassword
    },
    {
      path: '/new-password',
      name: 'New Password',
      component: pageNewPassword
    },
    {
      path: '/register',
      name: 'Register',
      component: pageRegister
    },
    {
      path: '/timeline',
      name: 'Timeline',
      component: pageTimeline
    }
  ]
})
