const express = require('express')
const secure = require('express-force-https')
const serveStatic = require('serve-static')
const path = require('path')

// create the express app
const app = express()
// force https
app.use(secure)
// Setup express middleware 'serveStatic' to serve static files
app.use("/", serveStatic ( path.join (__dirname, '/dist') ) )

const port = process.env.PORT || 5000
app.listen(port)
// Log to feedback that this is actually running
console.log('Server started on port ' + port)

/* If I refactor vue-router to use 'History' at a later date */
/*
  // Catch all routes and redirect to the index file
  app.get('*', function (req, res) {
      res.sendFile(__dirname + '/dist/index.html')
  })
*/
